#include <stdio.h>
#include <stdlib.h>

#include "apostolofShellFunctions.h"

int main(int argc, char *argv[]){
	if (argc == 1){
		interactiveMode();
	} else if (argc == 2) {
		batchFileMode(argv[1]);
	} else {
		printf("Not supported\n");
	}
	exit(EXIT_SUCCESS);
}