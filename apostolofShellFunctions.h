#ifndef APOSTOLOFSHELLFUNCTIONS_H_   /* Include guard */
#define APOSTOLOFSHELLFUNCTIONS_H_

//Read buffer size is the maximum number of characters (512) plus the newline character and the null
//terminator
#define BUFFER_SIZE 512 + 1 + 1
#define QUIT_CALLED 11880
//Cool colors
#define GRN  "\x1B[32m"
#define RESET "\x1B[0m"

const char semicolonDelimeter[2];
const char ampersandDelimeter[2];

//Function interactiveMode implements an interactive shell
void interactiveMode();

//Function batchFileMode runs a script file
void batchFileMode(char *filename);		//Filename and path of the script

//Function unconditionalRun splits a string on semicolon character (";") and runs each chunk
//unconditionally
void unconditionalRun(char *input,		//String to be split
	int *quit_called);					//A flag indicating if quit command was called

//Function conditionalRun splits a string on the and character ("&"), runs each chunk conditionally
//if the first character of the next chunk is also the and character and unconditionally otherwise
void conditionalRun(char *input,		//String to be split
	int *quit_called);					//A flag indicating if quit command was called

//Function execute creates a fork and runs a command using execvp
int execute(char *command,				//String containing the command
	char **argv);						//NULL terminated string array containing command's arguments

//Function parseCommand parses a command and its arguments out of a string
void parseCommand(char *input,			//String to be parsed
	char **command,						//String containing the command
	char ***argv,						//NULL terminated string array containing command's arguments
	int *argc);							//Number of arguments

//Function trimWhitespaces trims leading and trailing whitespace of a string
char *trimWhitespaces(char *string);	//String to be trimmed

#endif // APOSTOLOFSHELLFUNCTIONS_H_