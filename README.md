
# Shell

From [Wikipedia]:

>A Unix shell is a command-line interpreter or shell that provides a traditional Unix-like command line user interface. Users direct the operation of the computer by entering commands as text for a command line interpreter to execute, or by creating text scripts of one or more such commands.

## Repository

This repository provides an implementation of a simple Unix shell in C language. This shell was developed as part of the "Operating Systems" course at AUTH and has very limited functionality. All code in this repository is intended for academic and learning purposes only, not for everyday usage.

## Dependencies

Only a compiler is needed (e.g. gcc).

## Compilation

To compile, make sure all necessary packages and dependencies are installed. Then run:

```sh
$ make
```

## Usage

Run the shell in interactive mode with the command:
```sh
$ ./apostolofShell
```
or in batch mode using the command:
```sh
$ ./apostolofShell script
```
where **script** is the path and filename of the script you want to run.

**Free Software, Hell Yeah!**

[//]: # (Links)

   [Wikipedia]: <https://en.wikipedia.org/wiki/Unix_shell>
